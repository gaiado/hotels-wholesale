FROM mcr.microsoft.com/dotnet/aspnet:6.0-alpine AS base
WORKDIR /app
EXPOSE 80

#build back
FROM  mcr.microsoft.com/dotnet/sdk:6.0-alpine AS buildnet
WORKDIR /src
COPY back .
RUN dotnet restore 
RUN dotnet build "back.csproj" -c Release -o /app/build

FROM buildnet AS publish
RUN dotnet publish "back.csproj" -c Release -o /app/publish /p:UseAppHost=false

#build front
FROM node:16-alpine as buildvue
WORKDIR /src
COPY front .
RUN npm install
RUN npm run build

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
COPY --from=buildvue /src/dist /app/wwwroot
COPY --from=buildnet /src/LocalDatabase.db .
ENTRYPOINT ["dotnet", "back.dll"]
