﻿using System.Text;
using System;
using back.Models.Entities;
using back.Models;
using System.Text.Json;
using Newtonsoft.Json;
using back.Helpers;
using System.Reflection;

namespace back.Services
{
    public class PriceTravelApiService : IPriceTravelApiService
    {
        private readonly HttpClient _client;
        private readonly Context _context;
        private readonly QuoteDecoder _quoteEncrypt;

        public PriceTravelApiService(Context context)
        {
            _context = context;
            _client = new HttpClient()
            {
                BaseAddress = new Uri("https://ptapi.pricetravel.com")
            };
            _quoteEncrypt = new QuoteDecoder();
        }
        public async Task<TokenResponse> GetToken(int channelId)
        {
            var url = "/connect/token";
            var tokenRequest = _context.Channels.Find(channelId).GetTokenRequest();
            var response = await _client.PostAsync(url, new FormUrlEncodedContent(tokenRequest));
            if (response.IsSuccessStatusCode)
            {
                var stringResponse = await response.Content.ReadAsStringAsync();
                return System.Text.Json.JsonSerializer.Deserialize<TokenResponse>(stringResponse);
            }
            else
            {
                throw new HttpRequestException(response.ReasonPhrase);
            }
        }

        private async Task<string> GetSingleToken(int channelId)
        {
            Channel channel = _context.Channels.Find(channelId);
            if (channel.Expiration > DateTime.Now)
            {
                return channel.Token;
            }
            TokenResponse token = await GetToken(channelId);
            channel.Token = token.AccessToken;
            channel.Expiration = DateTime.Now.AddSeconds(token.ExpiresIn);
            _context.Channels.Update(channel);
            _context.SaveChanges();
            return channel.Token;
        }

        public async Task<string> GetHotelRates(HotelRateRequest hotelRateRequest)
        {
            var url = "/1.0/hotels/rates";
            string token = await GetSingleToken(hotelRateRequest.ChannelId);

            var data = new StringContent(hotelRateRequest.Json, Encoding.UTF8, "application/json");
            _client.DefaultRequestHeaders.Add("Accept", "application/json");
            _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var response = await _client.PostAsync(url, data);
            if (response.IsSuccessStatusCode)
            {
                var json = await response.Content.ReadAsStringAsync();
                HotelRate yourObject = JsonConvert.DeserializeObject<HotelRate>(json);

                foreach (Hotel hotel in yourObject.hotels)
                {
                    foreach (Room room in hotel.rooms)
                    {
                        foreach (Rate rate in room.rates)
                        {
                            var rote = rate.rateKey;
                            var decode = _quoteEncrypt.Decode(rote);
                            var finalDecode = JsonConvert.DeserializeObject<HotelDecode>(decode);
                            rate.contractId = finalDecode.contractId;
                        }
                    }
                }
                json = System.Text.Json.JsonSerializer.Serialize(yourObject);
                return json;
            }
            else
            {
                throw new HttpRequestException(response.ReasonPhrase);
            }
        }
    }
}
