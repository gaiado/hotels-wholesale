﻿namespace back.Models.Entities
{
    public class HotelRateRequest
    {
        public string Json { get; set; }
        public int ChannelId { get; set; }
    }
}
