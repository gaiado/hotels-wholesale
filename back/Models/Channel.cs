﻿using back.Models.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace back.Models
{
    public class Channel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string? ClientId { get; set; }
        public string? ClientSecret { get; set; }
        public string GrantType { get; set; }
        public string Scope { get; set; }
        public string? Token { get; set; }
        public DateTime? Expiration { get; set; }
        public bool Estado { get; set; }
        public KeyValuePair<string, string>[] GetTokenRequest()
        {
            return new[]
            {
                new KeyValuePair<string, string>("client_id", ClientId),
                new KeyValuePair<string, string>("client_secret", ClientSecret),
                new KeyValuePair<string, string>("grant_type", GrantType),
                new KeyValuePair<string, string>("scope", Scope)
            };
        }
    }
}
