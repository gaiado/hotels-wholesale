export interface RoomItem {
    channelName: string,
    nombre: string,
    mealPlan: string,
    contractId: string,
    neta: string,
    publica: string,
    margen: string
    currency: string
}