export interface Channel {
    id: number;
    name: string;
    clientId: string;
    clientSecret: string;
    grantType: string;
    scope: string;
    estado: boolean;
}