interface CancellationPolicy {
    startDate: Date;
    endDate: Date;
    amount: number;
    currency: string;
}

interface ReservationPolicy {
    type: string;
    code: string;
    description: string;
}

interface Detail {
    type: string;
    amount: number;
}

interface Taxes {
    total: number;
    details: Detail[];
}

interface FeesNotIncluded {
    total: number;
    details: any[];
    currency: string;
}

interface Rate {
    rateKey: string;
    contractId:string;
    clientCurrency: string;
    cancellationPolicies: CancellationPolicy[];
    reservationPolicies: ReservationPolicy[];
    amount: number;
    publicAmount: number;
    taxes: Taxes;
    feesNotIncluded: FeesNotIncluded;
}

interface Room {
    id: number;
    name: string;
    rates: Rate[];
}

export interface Hotel {
    rooms: Room[];
    id: number;
    name: string;
}

export interface QuoteResponse {
    hotel?: any;
    hotels: Hotel[];
    eventId: string;
    error?: any;
}