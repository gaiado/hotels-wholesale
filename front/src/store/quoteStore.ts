
import { defineStore } from 'pinia';
import { Hotel } from '~/interfaces/hotel';
import { Quote } from '~/interfaces/quote';
import { Rate } from '~/interfaces/rate';
import { getQuote } from '~/services/quotesService';
import { Channel } from '../interfaces/channel';
import { QuoteResponse } from '../interfaces/quoteResponse';
import { getActiveChannels, addChannel } from '../services/channelService';
import { getHotels } from '../services/hotelService';
import SourceMarket from '../assets/sourceMarket/sourceMarket.json';
import { RoomItem } from '../interfaces/roomItem';

interface QuoteState {
    channels: Channel[];
    quote: Quote;
    selectedChannels: Channel[];
    channel: Channel | undefined;
    adults: number;
    kids: number;
    kidsAge: [];
    hotel: Hotel;
    errors: Errors
    sourceMarket: {}
    listRooms: RoomItem[]
    qDates: [string, string],
    listRoomErrors: string[],
    loading: boolean,
    sort:boolean,
}

interface Errors {
    queryHotelid: string,
    kids: string,
    checking: string,
    checkout: string,
    hotels: boolean,
}
export const useQuoteStore = defineStore('quote', {
    state: (): QuoteState => ({
        channels: [],
        selectedChannels: [],
        loading: false,
        quote: {
            checkIn: "",
            checkOut: "",
            market: "MX",
            hotelIds: [],
            hotelRateFilter: { nonRefundable: false },
            roomCriteria: [{ paxes: [] }],
        },
        channel: undefined,
        adults: 2,
        kids: 0,
        kidsAge: [],
        hotel: {
            query: "",
            from: 0,
            placeTypes: '14',
            size: 10
        },
        errors: {
            queryHotelid: "",
            kids: "",
            checking: "",
            checkout: "",
            hotels: false,
        },
        sourceMarket: SourceMarket,
        listRooms: [],
        qDates: ['', ''],
        listRoomErrors: [],
        sort:false
    }),
    actions: {
        async loadChannels() {
            this.channels = [];
            const channels = await getActiveChannels();
            this.channels = channels;
        },
        async createQuotes() {
            this.loading = true;
            if (!this.validation()) {
                return this.loading = false
            }
            this.resetData();
            this.quote.hotelIds[0] = +this.quote.hotelIds[0];
            this.quote.roomCriteria[0].paxes = [];
            for (let i = 0; i < this.adults; i++) {
                this.quote.roomCriteria[0].paxes.push({ age: 18 });
            }
            for (let value of this.kidsAge) {

                if (value < 1) {
                    this.errors.kids = "Ingresar la edad minima de 0";
                    return false;
                }

                this.quote.roomCriteria[0].paxes.push({ age: +value || 1 });

            }
            let json = JSON.stringify(this.quote);
            if (this.channel !== undefined) {
                this.selectedChannels = [this.channel];
                let channelId = this.channel.id;
                await this.getRooms({
                    json, channelId
                }, "(" + this.selectedChannels[0].id + ") " + this.selectedChannels[0].name);
            } else {
                for (let value of this.channels) {
                    this.selectedChannels.push(value);
                    let channelId = value.id;
                    await this.getRooms({ json, channelId }, "(" + value.id + ") " + value.name);
                }
            }
            this.loading = false;

        },
        async selectHotels(query: string): Promise<[]> {
            this.hotel.query = query;
            const hotels = await getHotels(this.hotel);
            return hotels;
        },
        async getQuote(rate: Rate): Promise<QuoteResponse> {
            const data = await getQuote(rate);
            return data;
        },
        validation() {
            this.errors.kids = ""
            this.errors.queryHotelid = ""
            this.errors.checking = ""
            this.errors.checkout = ""

            if (this.quote.hotelIds.length < 1) {
                this.errors.queryHotelid = "Al menos llenar un campo";
            }
            if (this.quote.hotelIds == undefined || isNaN(this.quote.hotelIds[0])) {
                this.errors.queryHotelid = "Ingresar el ID";
            }

            if (this.quote.checkIn.length < 1 || this.quote.checkIn == "") {
                this.errors.checking = "Seleccionar una fecha";
            }
            if (this.quote.checkOut.length < 1 || this.quote.checkOut == "") {
                this.errors.checkout = "Seleccionar una fecha";
            }
            if (this.errors.queryHotelid != "" || this.errors.checking != "" || this.errors.checkout != "") {
                return false
            }

            return true;
        },
        async getRooms(rate: Rate, quote: string) {
            try {
                let response = await getQuote(rate);
                if (response.hotels.length < 1) {
                    this.listRoomErrors.push(quote);
                } else {
                    response.hotels[0].rooms.forEach(element => {
                        this.listRooms.push({ channelName: quote, nombre: element.name, contractId: element.rates[0].contractId, margen: this.getMargen(element.rates[0].publicAmount, element.rates[0].amount) + "", mealPlan: element.rates[0].reservationPolicies[0].description, neta: "" + element.rates[0].amount, publica: element.rates[0].publicAmount + "", currency: element.rates[0].clientCurrency })
                    });
                    this.sortingByName()
                }

            } catch (error) {
                this.listRoomErrors.push(quote);
            }

        },
        getMargen(publica: number, neta: number) {

            return Math.trunc(publica / neta * 100) - 100;
        },
        updateDates(dates: any[]) {
            if (dates != null) {
                this.quote.checkIn = dates[0].length > 0 && dates !== null ? dates[0] : '';
                this.quote.checkOut = dates[1].length > 0 && dates !== null ? dates[1] : '';
            } else {
                this.quote.checkIn = "";
                this.quote.checkOut = "";
            }
        },
        resetData() {
            this.listRooms = [];
            this.selectedChannels = [];
            this.listRoomErrors = [];
        },
        sortingByName() {

            this.listRooms.sort((a, b) => {
                let fa = a.nombre.toLowerCase(), fb = b.nombre.toLowerCase();
                if (fa < fb) {
                    return -1
                }
                if (fa > fb) {
                    return 1
                }
                return 0
            });
    },
    sortingByNeta() {
        this.listRooms.sort((a, b) => {
            let fa = a.publica.toLowerCase(), fb = b.publica.toLowerCase();
            if (fa < fb) {
                return -1
            }
            if (fa > fb) {
                return 1
            }
            return 0
        });
    }

}
});