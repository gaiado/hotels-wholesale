import { defineStore } from 'pinia';
import { Channel } from '../interfaces/channel';
import { getChannels, addChannel, updateChannel, deleteChannel } from '../services/channelService';

interface ChannelState {
    channels: Channel[];
    channel: Channel;
    dialog: boolean;
    selectedToRemove: number;
    errors: Errors
}

interface Errors {
    id: string,
    name: string,
    clientId: string,
    clientSecret: string,
    grantType: string,
    scope: string,
    estado: boolean,
}

export const useChannelStore = defineStore('channel', {
    state: (): ChannelState => ({
        channels: [],
        channel: {
            id: 0,
            name: "",
            clientId: "",
            clientSecret: "",
            grantType: "",
            scope: "",
            estado: true,
        },
        dialog: false,
        selectedToRemove: 0,
        errors: {
            id: "",
            name: "",
            clientId: "",
            clientSecret: "",
            grantType: "",
            scope: "",
            estado: true,
        }
    }),
    actions: {
        async loadChannels() {
            this.channels = [];
            const channels = await getChannels();
            this.channels = channels;
        },
        async saveChannel() {
            if (this.validation()) {
                await addChannel(this.channel);
                this.resetChannel();
                this.loadChannels();
            }
        },
        async removeChannel(channel: number) {
            this.dialog = false;
            this.selectedToRemove = 0;
            const success = await deleteChannel(channel);
            this.loadChannels();
        },
        async updateChannel(channel: Channel) {
            const success = await updateChannel(channel);

        },
        resetChannel() {
            this.channel = {
                id: 0,
                name: "",
                clientId: "",
                clientSecret: "",
                grantType: "",
                scope: "",
                estado: true,
            };
            this.resetErrors()
        },
        validation() {
            this.resetErrors();
            if (this.channel.name == "") {
                this.errors.name = 'El campo no puede estar vacio'
            }
            if (this.channel.clientId == "") {
                this.errors.clientId = 'El campo no puede estar vacio'
            }
            if (this.channel.clientSecret == "") {
                this.errors.clientSecret = 'El campo no puede estar vacio'
            }
            if (this.channel.grantType == "") {
                this.errors.grantType = 'El campo no puede estar vacio'
            }
            if (this.channel.scope == "") {
                this.errors.scope = 'El campo no puede estar vacio'
            }
            if (this.channel.id < 1) {
                this.errors.id = 'El campo no puede estar vacio'
            }
            if (this.errors.name != "" || this.errors.clientId != "" || this.errors.clientSecret != "" || this.errors.grantType != "" || this.errors.scope != "" || this.errors.id != "") {
                return false
            }

            return true;
        },
        resetErrors() {
            this.errors.id = ""
            this.errors.clientId = ""
            this.errors.clientSecret = ""
            this.errors.grantType = ""
            this.errors.name = ""
            this.errors.scope = ""
        }

    }
});
