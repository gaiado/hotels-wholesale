import axios from 'axios';
import { Hotel } from '../interfaces/hotel';

const channelApi = axios.create({
    baseURL: 'https://search-places.pricetravel.com/suggest'
});

const getHotels = async (hotel: Hotel): Promise<[]> => {
    const { data } = await channelApi.get('/hotel', { params: hotel });
    return data;
}
export {
    getHotels
};